#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ログファイルを通信毎に分割してログとタグを保存する
#
# 1通信は cCommCutString で分割される
#
# タグは最初の3文字。それに続くのは、タブかスペース
# それ以外はその他になる

import shutil
import sys
import os
import cProfile

cCommCutString = "===START COM==="
gProjectPath = None

def SetProjectDir(dir):
    global gProjectPath
    gProjectPath = dir
    print "gProjectPath:" + gProjectPath

def GetDirPath(comm):
    global gProjectPath

    if gProjectPath is None:
        print "ERROR, call SetProjectDir() first"
        exit()

    return os.path.join(gProjectPath, str(comm))

def GetTagFilePath(comm):
    #print "GetTagFilePath:" + os.path.join(GetDirPath(comm), "tags")
    return os.path.join(GetDirPath(comm), "tags")

def GetLogFilePath(comm):
    return os.path.join(GetDirPath(comm), "comm.log")

def GetLogComms():
    i = 1
    while os.path.exists(GetTagFilePath(i)):
        i = i + 1
    return i - 1

def prepareDir():
    for i in range(0,1000):
        dirPath = GetDirPath(i)
        if os.path.exists(dirPath):
            shutil.rmtree(dirPath)

def createSubDir(no):
    no = no + 1
    os.mkdir(GetDirPath(no))
    return no

def addTag(tag, tags):
    if tag not in tags:
        tags.append(tag)

gPrevTagNo = None
gPrevTagFile = None
def writeTags(no, tags):
    global gPrevTagNo, gPrevTagFile
    if not (gPrevTagNo == no):
        gPrevTagNo = no
        gPrevTagFile = open(GetTagFilePath(no), "w")
    for tag in tags:
        gPrevTagFile.write(tag + "\n")
    # f.close

def GetTime(line):
    if len(line) > 9:
        pos = line.find(" ")
        #print "pos:" + str(pos)
        if pos > 6:
            l = line.split(" ")[0]

            # check pantum type
            if l[0] == "[" and l[len(l) - 1] == "]":
                ll = l[1:(len(l) - 1)]
                #print "ll:'" + ll + "'"
                p1 = ll.find(":")
                if p1 > 0:
                    p2 = ll.find(":", p1 + 1)
                    if p2 > 0:
                        p3 = ll.find(".", p2 + 1)
                        if p3 > 0:
                            h = ll[0:p1]
                            m = ll[(p1+1):p2]
                            s = ll[(p2+1):p3]
                            ms = ll[(p3+1):]
                            lll = m + ":" + s + "." + ms
                            try:
                                h = int(h)
                                m = int(m)
                                s = int(s)
                                ms = int(ms)
                            except ValueError:
                                #print "time format 2:'" + l + "'"
                                return None
                            return lll
            # check oswrapper type
            spos = l.find(":")
            epos = l.find("'")
            #print "spos:" + str(spos) + " epos:" + str(epos)
            if epos == -1 or (l.find(".") > epos):
                #print "."
                epos = l.find(".")
            #print "spos:" + str(spos) + " epos:" + str(epos)
            if spos > 0 and epos > 0:
                m = l[0:spos]
                s = l[(spos+1):epos]
                ms = l[(epos+1):(epos+4)]
                ll = m + ":" + s + "." + ms
                #print "ll=[" + ll + "]"
                try:
                    m = int(m)
                    s = int(s)
                    ms = int(ms)
                except ValueError:
                    #print "time format 1:'" + l + "'"
                    return None
                return ll
            # check tera-term? type
            p1 = l.find(":")
            if p1 > 0:
                p2 = l.find(":", p1+1)
                if p2 > 0:
                    h = l[0:p1]
                    m = l[(p1+1):p2]
                    s = l[(p2+1):]
                    ll = m + ":" + s + ".000"
                    try:
                        #print "h:" + h + " m:" + m + " s:" + s
                        h = int(h)
                        m = int(m)
                        s = int(s)
                    except ValueError:
                        #print "time format 3:'" + l + "'"
                        return None
                    return ll
    return None

def GetTag(line):
    if len(line) < 5:
        return None

    if GetTime(line) == None:
        pos = 0
    else:
        pos = line.find(" ") + 1
        if (pos > line.find("\t")):
            if (line.find("\t") > 0):
                pos = line.find("\t") + 1
    #print "line:" + line
    #print "pos:" + str(pos) + " '" + line[pos+3] + "'"
    if len(line) < pos + 3:
        return None
    tagExists = (line[pos+3] == '\t' or line[pos+3] == ' ')
    if tagExists:
        tag = line[(pos+0):(pos+3)]
        #print "TAG:" + tag
        return tag.rstrip()
    return None

def divideMonacoLog(f):
    if isinstance(f, str) or isinstance(f, unicode):
        file = open(f, 'r')
    else:
        file = f

    gPrevTagNo = -1

    prevLogNo = -1
    prevLogFile = None
    prepareDir()
    no = createSubDir(0)
    tags = []

    for line in file:
        l = line.rstrip()
        tag = GetTag(l)
        if not (tag is None):
            addTag(tag, tags)
        if not(prevLogNo == no):
            prevLogNo = no
            prevLogFile = open(GetLogFilePath(no), "w")
        prevLogFile.write(l + "\n")
        # f.close()

        if cCommCutString in l:
            writeTags(no, tags)
            no = createSubDir(no)
    writeTags(no, tags)

if __name__ == "__main__":
    argc = len(sys.argv)
    if argc == 1:
        file = sys.stdin
        print "cut log from stdin"
    else:
        file = open(sys.argv[1], 'r')
        print "cut log from", sys.argv[1]

    SetProjectDir("/tmp")
    #cProfile.run('divideMonacoLog(file)')
    divideMonacoLog(file)
    print "end of", sys.argv[0]
