#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from Tkinter import *
import tkFileDialog
from tkColorChooser import askcolor
import tkSimpleDialog
import tkMessageBox
import cut_log
from ScrolledText import *
import platform
import os
import ttk
import fontChooser
import shutil
import webbrowser
from os.path import expanduser

gTagSwitch = {}

gTags = [ "MUI", "FXM", "REP", "FAX", "T30", "T3T", "MDM", "DAT", "CDC", "ECM", "FRM", "QBK", "BUF", "PRM", "OSW", "others"]
gColors = [ "GREEN", "YELLOW", "ORANGE", "PURPLE", "PINK", "BLUE", "LIGHT GREEN", "LIGHT PINK", "LIGHT BLUE", "CYAN", "GRAY", "BROWN", "TOMATO", "TAN", "WHITE", "PINK" ]
gDefaultTags = [ "MUI", "FXM", "FAX", "T30", "T3T", "MDM", "DAT", "CDC", "ECM" ]

gSpin = None
gSpinBox = None
gLog = None
gFindText = None
gLastFindText = ""
gLastFindTextPos = "1.0"
gLastFindTextPosEnd = "1.0"
gLastSelectedLine = ""

gElapsedTime = ""

gProjectPath = expanduser("~") + "/.monalog/"
if os.path.exists(gProjectPath) == False:
    os.mkdir(gProjectPath)

gProblemPath = None

gFindHistory = []

gLastTime = None

gFontName = u"u'MSゴシック'"

gFontSize = 12
gBackground = "BLACK"

gContextMenu = None
gPointedTextPos = None

gMarkList = None
gLineNo = []
gMarked = []
gJumpTable = []

def selectAll():
    global gTags, gTagSwitch
    for tag in gTags:
        gTagSwitch[tag].set(True)
    update()

def selectNone():
    global gTags, gTagSwitch
    for tag in gTags:
        gTagSwitch[tag].set(False)
    update()

def selectStandard():
    global gTags, gTagSwitch
    for tag in gTags:
        gTagSwitch[tag].set(tag in gDefaultTags)
    update()

def selectReverse():
    global gTags, gTagSwitch
    for tag in gTags:
        if gTagSwitch[tag].get():
            gTagSwitch[tag].set(False)
        else:
            gTagSwitch[tag].set(True)
    update()

def changeFont(root):
    font = fontChooser.askChooseFont(root)
    print "font:" + font

def setCommNo():
    global gSpinBox
    no = cut_log.GetLogComms()
    print "last comm:" + str(no)
    gSpinBox.configure(to=no)

def openProblem():
    global gProjectPath
    global gProblemPath

    dir = tkFileDialog.askdirectory(title="Choose Project Directory", initialdir=gProjectPath)
    if not (dir == ""):
        if os.path.exists(os.path.join(dir, "monaco.log")):
            gProblemPath = dir
            cut_log.SetProjectDir(dir)
            setCommNo()
            update()
        else:
            tkMessageBox.showerror("Project Error", "Not a monaco project directory")

def createNewProblem():
    global gProjectPath
    global gProblemPath

    text = tkSimpleDialog.askstring("Problem Name", "entry problem name(directory name)")
    if not (text is None):
        baseDir = os.path.join(gProjectPath, text)
        if os.path.exists(baseDir):
            tkMessageBox.showerror('New Problem', 'Same name is already used')
        else:
            os.mkdir(baseDir)
            fileType = [('monaco log file','.*'),
                    ('log file','.log'),
                    ('text file', '.text'),
                    ('text file', '.txt')]
            fileName = tkFileDialog.askopenfilename(filetypes=fileType, title="Choose Monaco Log File")
            if len(fileName) > 0:
                cut_log.SetProjectDir(baseDir)
                cut_log.divideMonacoLog(fileName)
                shutil.copy(fileName, os.path.join(baseDir, "monaco.log"))
                gProblemPath = baseDir
                setCommNo()
                update()

def createMenu(root):
    rootMenu = Menu(root)
    root.configure(menu = rootMenu)
    fileMenu = Menu(rootMenu)
    rootMenu.add_cascade(label='Problem', menu=fileMenu, underline=0)
    fileMenu.add_command(label='New Problem...', under=0, command=createNewProblem)
    fileMenu.add_command(label='Open Problem...', under=0, command=openProblem)
    fileMenu.add_separator()
    fileMenu.add_command(label="Save Settings", under=0, command=saveSettings)
    fileMenu.add_separator()
    fileMenu.add_command(label='Quit', under=0, command=lambda: quitProgram(root))
    toolMenu = Menu(rootMenu)
    rootMenu.add_cascade(label='Selection', menu=toolMenu, underline=0)
    toolMenu.add_command(label='Select All', under=0, command=selectAll)
    toolMenu.add_command(label='Select None', under=0, command=selectNone)
    toolMenu.add_command(label='Select Standard', under=0, command=selectStandard)
    toolMenu.add_command(label='Reverse Selections', under=0, command=selectReverse)
    #toolMenu.add_separator()
    #toolMenu.add_command(label='Change Font..', under=0, command=lambda: changeFont(root))
    helpMenu = Menu(rootMenu)
    rootMenu.add_cascade(label='Help', menu=helpMenu, underline=0)
    helpMenu.add_command(label='Version 1.00', under=0)
    helpMenu.add_command(label='Help...', under=0, command=lambda: webbrowser.open('http://bitbucket.org/baliset2010/monaco-log/overview'))

def changeTagColor(tag, color):
    global gColor, gLog, gTags
    for i in range(0, len(gColors)):
        if tag == gTags[i]:
            gColors[i] = color[1]
            print "tag:" + tag + " to " + color[1]
            gLog.tag_config(gTags[i], foreground=gColors[i])

def changeColor():
    global gLog, gPointedTextPos
    color = askcolor()
    if not (color is None):
        lno, char = gPointedTextPos.split(".")
        line = gLog.get(lno + ".0", lno + ".end")
        tag = cut_log.GetTag(line)
        if tag is None:
            tag = "OTH"
        changeTagColor(tag, color)

def changeBackgroundColor():
    global gLog, gBackground
    color = askcolor()
    if not (color is None):
        gLog.configure(background=color[1])
        gBackground = color[1]

def saveMarkList():
    global gSpin, gMarked, gLineNo
    comm = gSpin.get()
    dir = cut_log.GetDirPath(comm)
    f = open(os.path.join(dir, "mark"), "w")
    for mark in gMarked:
        f.write(str(mark) + "\n")
    f.close()

def readMarkList():
    global gSpin, gMarked
    comm = gSpin.get()
    dir = cut_log.GetDirPath(comm)
    fileName = os.path.join(dir, "mark")
    if os.path.exists(fileName):
        f = open(fileName,"r")
        gMarked = []
        for line in f.readlines():
            gMarked.append(int(line.rstrip()))
        f.close()
    else:
        gMarked = []

def markThisLine():
    global gPointedTextPos, gLineNo, gMarked, gLog, gMarkList, gJumpTable
    lno, char = gPointedTextPos.split(".")
    lineNo = gLineNo[int(lno)]
    #print "lineNo:" + str(lineNo)
    if not (lineNo in gMarked):
        gMarked.append(lineNo)
        gLog.tag_add("marked", lno + ".0", lno + ".end")
        line = gLog.get(lno + ".0", lno + ".end")
        gJumpTable.append(str(lno) + " " + line)
        saveMarkList()
        gMarkList.configure(values=gJumpTable)

def unmarkThisLine():
    global gPointedTextPos, gLineNo, gMarked, gLog, gJumpTable
    lno, char = gPointedTextPos.split(".")
    lineNo = gLineNo[int(lno)]
    if lineNo in gMarked:
        gMarked.remove(lineNo)
        gLog.tag_remove("marked", lno + ".0", lno + ".end")

        line = gLog.get(lno + ".0", lno + ".end")
        line = str(lno) + " " + line
        jt = gJumpTable
        gJumpTable = []
        for l in jt:
            print "line:" + line
            print "   l:" + l
            if not (line == l):
                gJumpTable.append(l)
            #else:
            #    print "same found"
        gMarkList.configure(values=gJumpTable)
        saveMarkList()

def popupMenu(event):
    global gContextMenu, gLog, gPointedTextPos
    gPointedTextPos = gLog.index("@%s,%s" % (event.x, event.y))
    #print "point:" + gPointedTextPos
    gContextMenu.post(event.x_root, event.y_root)

def createContextMenu(root):
    global gContextMenu, gLog
    gContextMenu = Menu(root,tearoff=0)
    gContextMenu.add_command(label="change color", command=changeColor)
    gContextMenu.add_command(label="change background color", command=changeBackgroundColor)
    gContextMenu.add_separator()
    gContextMenu.add_command(label="mark this line", command=markThisLine)
    gContextMenu.add_command(label="unmark this line", command=unmarkThisLine)
    if platform.system() == "Windows":
        gLog.bind("<Button-3>", popupMenu)
    else:
        gLog.bind("<Button-2>", popupMenu)

def update():
    global gLog, gLog, gTags, gTagSwitch, gSpinBox, gProblemPath, gMarked, gJumpTable

    if gLog is None:
        return

    if gProblemPath is None:
        return

    print "problem:" + gProblemPath
    cut_log.SetProjectDir(gProblemPath)
    readMarkList()
    fileName = cut_log.GetLogFilePath(gSpinBox.get())
    print "update: " + fileName
    if os.path.exists(fileName):
        gJumpTable = []
        gLog.configure(state=NORMAL)
        gLog.delete('1.0', 'end')
        no = 0
        lineNo = 0
        for line in open(fileName, 'r'):
            l = line.rstrip() + "\n"
            tag = cut_log.GetTag(l)
            shouldShow = False
            if tag in gTags:
                if gTagSwitch[tag].get():
                    shouldShow = True
            else:
                if gTagSwitch["others"].get():
                    shouldShow = True
                    tag = "others"
            if ":ERROR:" in l:
                tag = "RED"
                shouldShow = True
            if ":WARNING:" in l:
                tag = "RED"
                shouldShow = True
            if l.startswith("REP"):
                tag = "GREEN"
                shouldShow = True

            if shouldShow:
                gLog.insert('end', l, tag)
                gLineNo.append(no)
                if no in gMarked:
                    gLog.tag_add("marked", str(lineNo) + ".0", str(lineNo) + ".end")
                    gJumpTable.append(str(lineNo) + " " + l)
                lineNo = lineNo + 1
            no = no + 1
        gLog.configure(state=DISABLED)
        gMarkList.configure(values=gJumpTable)

def addFindHistory(t):
    global gFindHistory, gFindText

    if not (t in gFindHistory):
       gFindHistory.append(t)
    gFindText.configure(values=gFindHistory)

def findWordForward(t):
    global gLog, gLastFindText, gLastFindTextPos, gLastFindTextPosEnd

    print "pos:" + gLastFindTextPos + " " + gLastFindText
    if t == gLastFindText:
        tt = gLastFindTextPos.split(".")
        pos = tt[0] + "." + str(int(tt[1]) + len(gLastFindText))
    else:
        addFindHistory(t)
        pos = "1.0"
    print pos

    countVar = StringVar()
    pos = gLog.search(t, pos, stopindex="end", count=countVar)
    if not (pos == ""):
        print "find:" + pos
        try:
            gLog.tag_remove("search", gLastFindTextPos, gLastFindTextPosEnd)
        except UnboundLocalError:
            pass
        gLog.mark_set("insert", pos)
        gLog.see("insert")
        gLastFindTextPos = pos
        gLastFindTextPosEnd = "%s + %sc" % (pos, countVar.get())
        gLog.tag_add("search", gLastFindTextPos, gLastFindTextPosEnd)
        gLastFindTextPos = pos
        gLastFindText = t
    else:
        print chr(7)

def findWordBackward(t):
    global gLog, gLastFindText, gLastFindTextPos, gLastFindTextPosEnd

    print "pos:" + gLastFindTextPos + " " + gLastFindText
    if t == gLastFindText:
        tt = gLastFindTextPos.split(".")
        if int(tt[1]) > len(gLastFindText):
            pos = tt[0] + "." + str(int(tt[1]) - len(gLastFindText))
        else:
            pos = tt[0] + ".0"
    else:
        addFindHistory(t)
        pos = "end"
    print pos

    countVar = StringVar()
    pos = gLog.search(t, pos, stopindex="1.0", count=countVar, backwards=True)
    if not (pos == ""):
        print "find:" + pos
        try:
            gLog.tag_remove("search", gLastFindTextPos, gLastFindTextPosEnd)
        except UnboundLocalError:
            pass
        gLog.mark_set("insert", pos)
        gLog.see("insert")
        gLastFindTextPos = pos
        gLastFindTextPosEnd = "%s + %sc" % (pos, countVar.get())
        gLog.tag_add("search", gLastFindTextPos, gLastFindTextPosEnd)
        gLastFindText = t
    else:
        print chr(7)

def findNext():
    t = gFindText.get()
    if len(t) > 0:
        findWordForward(t)

def findPrev():
    t = gFindText.get()
    if len(t) > 0:
        findWordBackward(t)

def getMsec(tm):
    p1 = tm.find(":")
    p2 = tm.find(".")
    m = int(tm[0:p1])
    s = int(tm[(p1+1):p2])
    ms = int(tm[(p2+1):])
    return (m * 60 * 1000) + (s * 1000) + ms

def setDuration(tm):
    global gLastTime, gElapsedTime

    t1 = getMsec(gLastTime)
    t2 = getMsec(tm)
    #print "t1:" + str(t1) + " t2:" + str(t2)
    if t2 >= t1:
        sign = "+"
        t = t2 - t1
    else:
        sign = "-"
        t = t1 - t2
    ts = "%s%d:%02d:%03d" % (sign, t / 60 / 1000, (t / 1000) % 60, t % 1000)
    gElapsedTime.set(ts) 

def clickText(event):
    global gLog, gLastSelectedLine, gLastTime

    try:
        gLog.tag_remove("select", "1.0", "end")
    except UnboundLocalError:
        pass
    index = gLog.index("@%s,%s" % (event.x, event.y))
    gLastSelectedLine, char = index.split(".")
    gLog.tag_add("select", gLastSelectedLine + ".0", gLastSelectedLine + ".end")
    line = gLog.get(gLastSelectedLine + ".0", gLastSelectedLine + ".end")
    tm = cut_log.GetTime(line)
    if not (tm is None):
        print "tm:'" + tm + "'"
        if not (gLastTime is None):
            setDuration(tm)
        gLastTime = tm

def clearFind():
    global gFindText
    gFindText.set('')

def jump(event):
    global gMarkList, gLog
    t = gMarkList.get()
    t = t.split(" ")[0]
    gLog.see(t + ".0")

def buildGui(root):
    global gTagSwitch, gSpin, gSpinBox, gLog, gTags, gColors, gFindText, gElapsedTime
    global gBackground, gMarkList

    f0 = Frame(root)
    Label(f0, text="comm.").pack(side=LEFT,anchor='e')
    gSpin = StringVar()
    gSpinBox = Spinbox(f0, from_=1, to=10, increment=1,width=2,textvariable=gSpin, command=update)
    gSpinBox.pack(side=LEFT)
    Label(f0, text="Find",width=8,anchor='e').pack(side=LEFT)
    #gFindText = Entry(f0, width=10)
    gFindText = ttk.Combobox(f0, width=10, values=tuple(gFindHistory))
    gFindText.pack(side=LEFT)
    cbtn = Button(f0, text="X", command=clearFind)
    cbtn.config(font=('helvetica', 8))
    cbtn.pack(side=LEFT)

    #if platform.system() == "Darwin":
    #    Button(f0, text="DN", command=findNext).pack(side=LEFT)
    #    Button(f0, text="UP", command=findPrev).pack(side=LEFT)
    #else:
    #    upImage = PhotoImage(file="../images/up.gif")
    #    downImage = PhotoImage(file="../images/down.gif")
    #    Button(f0, image=downImage, command=findNext).pack(side=LEFT)
    #    Button(f0, image=upImage, command=findPrev).pack(side=LEFT)
    Button(f0, text="DN", command=findNext).pack(side=LEFT)
    Button(f0, text="UP", command=findPrev).pack(side=LEFT)

    gElapsedTime = StringVar()
    Label(f0, textvariable=gElapsedTime).pack(side=LEFT)

    gMarkList = ttk.Combobox(f0, values=tuple())
    gMarkList.pack(side=LEFT)
    gMarkList.bind("<<ComboboxSelected>>", jump)

    f1 = Frame(root)
    for tag in gTags:
        gTagSwitch[tag] = BooleanVar()
        gTagSwitch[tag].set(tag in gDefaultTags)
        chk = Checkbutton(f1, text=tag, variable=gTagSwitch[tag],command=update)
        chk.pack(side=LEFT)

    f2 = Frame(root)
    if os.name == "posix":
        #NG:gLog = ScrolledText(f2, background=gBackground, font=("tkFixedFont", gFontSize), wrap=NONE)
        gLog = ScrolledText(f2, background=gBackground, font=("courier", gFontSize), wrap=NONE)
    else:
        gLog = ScrolledText(f2, background=gBackground, font=(gFontName, gFontSize), wrap=NONE)
    for i in range(0, len(gTags)):
        print "tag:" + gTags[i] + " col:" + gColors[i]
        gLog.tag_config(gTags[i], foreground=gColors[i])
    gLog.tag_config("RED", foreground="RED")
    gLog.tag_config("BLUE", foreground="BLUE")
    gLog.tag_config("GREEN", foreground="GREEN")
    gLog.tag_config("search", background="green")
    gLog.tag_config("select", background="blue")
    gLog.tag_config("marked", background="yellow")
    gLog.pack(side=LEFT, expand=True, fill=BOTH)
    gLog.bind("<1>", clickText)

    f0.pack(fill=X)
    f1.pack(fill=X)
    f2.pack(fill=BOTH, expand=True, pady=2, padx=2)

def saveLastPath():
    global gProblemPath, gTags, gSpinBox

    if not (gProblemPath is None):
		f = open(".last", "w")
		f.write("project=" + gProblemPath + "\n")
		f.write("comm=" + str(gSpinBox.get()) + "\n")
		f.close()

def retrieveLastPath():
    global gProblemPath, gTags, gSpin, gTagSwitch
  
    if os.path.exists(".last"):
        f = open(".last", "r")
        lines = f.readlines()
        for line in lines:
            l = line.rstrip()
            if l.startswith("project="):
                gProblemPath = l[8:]
            elif l.startswith("comm="):
                gSpin.set(int(l[5:]))
        f.close()

def saveFindHistory():
    global gFindHistory

    f = open(".finds", "w")
    for l in gFindHistory:
        f.write(l + "\n")
    f.close()

def retrieveFindHistory():
    global gFindHistory

    gFindHistory = []
    gFindHistory.append(":ERROR:")
    gFindHistory.append(":WARNING:")

    if os.path.exists(".finds"):
        f = open(".finds", "r")
        for l in f.readlines():
            l = l.rstrip()
            if l != ":WARNING:" and l != ":ERROR:":
                gFindHistory.append(l)

def quitProgram(root):
    saveLastPath()
    saveFindHistory()
    root.destroy()

def saveSettings():
    global gTags, gColors, gFontName, gFontSize, gBackground
    f = open("colors.sav", "w")
    f.write("background=" + gBackground + "\n")
    for i in range(0, len(gTags)):
        tag = gTags[i]
        color = gColors[i]
        f.write(tag + "=" + color + "\n")
    f.close()

def readSettings():
    global gTags, gColors, gFontName, gFontSize, gBackground

    f = None
    if os.path.exists("colors.sav"):
        f = open("colors.sav", "r")
    elif os.path.exists("colors.txt"):
        f = open("colors.txt", "r")
        
    if not (f is None):
        gTags = []
        gColors = []
        for line in f.readlines():
            line = line.rstrip()
            l = line.split("=")
            if len(l) == 2:
                if l[0] == "background":
                    gBackground = l[1]
                else:
                    gTags.append(l[0])
                    gColors.append(l[1])
        f.close()
        if not ("others" in gTags):
            gTags.append("others")
            gColors.append("GREEN")

    if os.path.exists("font.txt"):
        f = open("font.txt", "r")
        for line in f.readlines():
            line = line.rstrip()
            l = line.split("=")
            if l[0] == "font":
                gFontName = l[1]
            elif l[0] == "fontSize":
                gFontSize = int(l[1])
        f.close()

if __name__ == '__main__':
    root = Tk()
    root.option_add("*Font", "TkFixedFont")
    root.title("monaco log viewer 1.01")
    
    readSettings()

    retrieveFindHistory()
    buildGui(root)
    createMenu(root)
    createContextMenu(root)

    retrieveLastPath()
    update()

    root.mainloop()

