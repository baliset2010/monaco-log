#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ログファイルから解析する
#

import os
import sys
import cut_log

def getTags():
    tags = [ "FAX", "T30", "T3T", "MDM", "DAT", "CDC", "ECM", "FRM", "EVT",
            "AT ", "QBK", "BUF", "PRM", "OSW"]
    return tags

def getTagLogFile(tag, no):
    return os.path.join(cut_log.GetDirPath(no), tag.rstrip())

def divideLogFileByTag():
    tags = getTags()
    lastComm = cut_log.GetLogComms()
    for no in range(1, lastComm):
        for tag in tags:
            fin = open(cut_log.GetLogFilePath(no), "r")
            fout = open(getTagLogFile(tag, no), "w")
            for l in fin.readlines():
                lineTag = cut_log.GetTag(l.rstrip())
                if tag == lineTag:
                    fout.write(l)
            fin.close()
            fout.close()

        lastText = ",textcolor=\"teal\",linecolor=\"teal\"];\n"
        fin = open(cut_log.GetLogFilePath(no), "r")
        fout = open(getTagLogFile("SEQ", no), "w")
        result = "Unknown"
        fout.write("msc {\n")
        fout.write("\tUI,FAX,T30,DAT,ECM,MDM;\n")
        for l in fin.readlines():
            if l.startswith("@"):
                l = l.rstrip()[1:]
                if l == "Ring On":
                    fout.write("\tMDM->MDM [label=\"Ring On\"];\n")
                elif l == "Ring Off":
                    fout.write("\tMDM->MDM [label=\"Ring Off\"];\n")
                elif l.startswith("---"):
                    if "label=" in l:
                        fout.write("\t" + l)
                    else:
                        l1 = l[0:4]
                        l2 = l[4:]
                        fout.write("\t" + l1 + " [label=\"" + l2 + "\"];\n")
                else:
                    pos = l.find(" ")
                    l1 = l[0:pos]
                    l2 = l[(pos+1):]
                    fout.write("\t" + l1 + " [label=\"" + l2.rstrip() + "\"];\n");
            if l.startswith("\<E\>"):
                fout.write("\t--- [label=\"Decode Error\"" + lastText)
            if "REP" in l:
                if "Result" in l:
                    result = l.split(": ")[1]
        fout.write("\t--- [label=\"Result " + result.rstrip() + "\"" + lastText)
        fout.write("}\n")
        fin.close()
        fout.close()

def findError(no):
    fin = open(cut_log.GetLogFilePath(no), "r")
    decodeError = False
    for l in fin.readlines():
        if ":ERROR:" in l:
            print "Error in comm " + str(no)
        if "<E>" in l:
            decodeError = True
        if "REP" in l:
            if "Result" in l:
                print l.rstrip() + " in comm " + str(no)

    if decodeError:
        print "Decode Error in comm " + str(no)

def analyze():
    for no in range(1, cut_log.GetLogComms()):
        findError(no)

if __name__ == "__main__":
    argc = len(sys.argv)
    if argc == 1:
        file = sys.stdin
        print "cut log from stdin"
    else:
        file = open(sys.argv[1], 'r')
        print "cut log from", sys.argv[1]

    cut_log.divideMonacoLog(file)
    divideLogFileByTag()
    analyze()

