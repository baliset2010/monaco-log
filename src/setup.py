#!/usr/bin/env python
# -*- coding: utf-8 -*-
# setup.py

try:
	try:
		import py2exe.mf as modulefinder
	except ImportError:
		import modulefinder
	import win32com, sys
	for p in win32com.__path__[1:]:
		modulefinder.AddPackagePath("win32com", p)
	for extra in ["win32com.shell"]: #,"win32com.mapi"
		__import__(extra)
		m = sys.modules[extra]
		for p in m.__path__[1:]:
			modulefinder.AddPackagePath(extra, p)
except ImportError:
	# no build path setup, no worries
	pass


from distutils.core import setup
import py2exe


#includes = [ 'monalog' ]
includes = []
excludes = [ '_gtkagg', 'bsddb', 'curses', 'email', 'pywin.debugger',
	'pywin.debugger.dbgcon', 'pywin.dialogs' ]
packages = []
dll_excludes = ['libgdk-win32-2.0.0.dll', 'libgobject-2.0.0.dll', 'mswsock.dll', 'powrprof.dll' ]

option = {
	"compressed"	: 1,
	"optimize"		: 2,
	"bundle_files"	: 1,
}

setup(
	options = {
		"py2exe": {"compressed" : 1,
				   "optimize" : 2,
				   "includes" : includes,
				   "excludes" : excludes,
				   "dll_excludes" : dll_excludes,
				   "bundle_files" : 1,
				   "dist_dir" : "dist",
				   "xref" : False,
				   "skip_archive" : False,
				   "ascii" : False,
				   "custom_boot_script" : '',
				   }
	},

	py_modules = [ 'cut_log' ],

	windows = [ "monalog.py" ],

	zipfile = None
)

