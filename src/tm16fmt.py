#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ログファイルについた16進を時間に変換する

import sys

if __name__ == "__main__":
    argc = len(sys.argv)
    if argc == 1:
        file = sys.stdin
    else:
        file = open(sys.argv[1], 'r')

    lines = file.readlines()
    for line in lines:
        l = line.rstrip()
        s = None
        if len(l) > 8 and l[8] == ':':
            try:
                t = int(l[0:8], 16)
                # _TIME_STAMP_LOG will use gettimeofday() which returns usec
                t = t / 1000
                h = int(t / 1000 / 60 / 60)
                m = int(t / 1000 / 60 % 60)
                s = int(t / 1000 % 60)
                ms = int(t % 1000)
                #s = "{0}:{1:0>2}:{2:0>2}.{3:0>3}".format(h,m,s,ms)
                s = "{0}:{1:0>2}.{2:0>3}".format(m,s,ms)
            except ValueError:
                pass
        if s == None:
            print l
        else:
            print s.rstrip() + "\t" + l[9:]

