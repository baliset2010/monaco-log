#monaco log viewer

This tool is for viewing monaco log. 'monaco' is a fax software module for embedded system (usually used for MFP, Multi Function Printer) from [Bitcom](http://www.bitcom-net.com). 

This tool is NOT product from Bitcom, just personal study for Python and Tkinter (a development tool). Use this as your own risk.

## How to Install

### Windows

1. Download [zip file](https://bitbucket.org/baliset2010/monaco-log/downloads)
2. extract zip file anywhere you want to place.

### Mac & Linux

1. install python (2.7 recommended)
2. download repository
3. place monalog.py & cut_log.py anywhere you like

## How to launch

### Windows

1. click 'monalog' or 'monalog.exe', depends on the option settings in explorer.

### Mac & Linux

1. python monalog.py

## Usage

### Create Problem

To view log, you need to create a problem first. A problem contains the log from monaco system and and its database.

1. Select 'New Problem' from 'Problem' Menu.
2. Name problem name, this will be used for new problem directory name.
3. Specify a log from monaco system.

New problem was created. Later, you can use it by 'Open Problem' again.

*Base ProblemDirectory* is meant to be a base directory of analyzed logs. Currenlty .monalog directory will be created under the home directory.

*Problem Name* is meant to be ID for the problem you want to analyze. You may specify the problem case which can be used for directory, or just use inspection date, or just sequence number. The contents of the problem is a directory, so it is good idea to put some memo under the problem directory, so you can remember what you had thought when you inspected the log, later.

### View Log

Each module has 3-letter-ID, and they can be display/hide by check/uncheck on the top of the log viewer. If ID has not found, temporary ID 'others' is given. Each message of ID has it own color. And can be changed by right click on the line. Also, the background color can be changed by the same way.

Changing colors are NOT saved automatically, by 'Save Setting' from 'Problem' Menu. You can reset it by deleting the file 'colors.txt' where the same directory of the program.

By default, several ID are enabled. You can change by click the check box, or from the menu.

If the log contains more than one communication, the log will be divided as it contains only one communication. Viewing previous/next log can be changed by clicking the number of 'comm.' at the top of the window.

#### Find

Find words by entering the find box and click 'DN'(DOWN) or 'UP'(UP) button. Also select words by click the down arrow key to choose find-history-word. Please remember that it will NOT search in hidden lines.

#### Mark

From right click menu, you can mark/unmark the line. The marked lines are showed at the next box of 'Find', by selecting the item, you can jump to the line to show.

#### Duration

If the log contains time at the top of the each line, you can see the duration at the right of 'Find' box, by clicking 2 lines.

Time format only recognized are,...

1. MM:SS.mmm
2. MM:SS'mmm
3. HH:MM:SS
4. [HH:MM:SS.mmm]


## Windows Build

see src/setup.py
